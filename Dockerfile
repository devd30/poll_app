FROM devd/ubuntu-msql

MAINTAINER davesh

RUN apt-get clean && apt-get update 
RUN apt-get install git -y
RUN apt-get install python-pip -y \
    python \
    libmysqlclient-dev

WORKDIR /

RUN rm -rf poll_app

RUN git clone https://devd30:MbnU3hNEJCuaqLPWxeTY@bitbucket.org/devd30/poll_app.git

RUN ls 
RUN pip install -r /poll_app/requirements.txt
 
#RUN pip install -r poll_app/requirements.txt

EXPOSE 80 8000 

RUN service mysql restart

WORKDIR /poll_app

CMD source poll_app/bin/activate

CMD python Events/webapp/manage.py migrate

CMD python Events/webapp/manage.py runserver 0.0.0.0:8000



#WORKDIR poll_app

#CMD source poll_app/bin/activate

#CMD python poll_app/Events/webapp/manage.py migrate

#CMD python poll_app/Events/webapp/manage.py runserver 0.0.0.0:8000


